
enum SIZE {
    //% block="29*29"
    1,
    //% block="58*58"
    2
}

enum LINE {
    //% block="1"
    1,
    //% block="2"
    2,
    //% block="3"
    3,
    //% block="4"
    4
}


//% color="#AA278D" iconWidth=50 iconHeight=40
namespace  PAJ7620U2_Gesture_Sensor {

    //% block="PAJ7620U2 initialization set Model to [MODEL]" blockType="command"
    //% MODEL.shadow="dropdown" MODEL.options="BTN" 
    export function  setGestureRate(parameter: any, block: any) {
        let model = parameter.MODEL.code;
        Generator.addInclude('DFRobot_PAJ7620U2.h', '#include <DFRobot_PAJ7620U2.h>');
        Generator.addObject(`DFRobot_PAJ7620U2`, `DFRobot_PAJ7620U2`, `paj;`);
        Generator.addSetup(`PAJ7620U2.begin`, `paj.begin();`);
        Generator.addCode(`paj.setGestureHighRate(${model});`);
    }

     //% block="PAJ7620U2 Must be selected before triggering" blockType="command"
     export function  gesture(parameter: any, block: any) {
        
        Generator.addCode(`DFRobot_PAJ7620U2::eGesture_t gesture = paj.getGesture();`);
    }

    //% block="PAJ7620U2 [GESTURE] triggered?" blockType="boolean"
    //% GESTURE.shadow="dropdown" GESTURE.options="GT"
    export function  getGesture(parameter: any, block: any) {
        let gest = parameter.GESTURE.code;
        Generator.addCode([`(paj.gestureDescription(gesture)=="${gest}")`,Generator.ORDER_UNARY_POSTFIX]);
    }
/*   这个block能用，但是不好用，就注释掉了，因为它会一直返回None,频率很快
    //% block="PAJ7620U2 result" blockType="reporter"
    export function  return_Gesture(parameter: any, block: any) {
        
        Generator.addCode([`paj.gestureDescription(paj.getGesture())`,Generator.ORDER_UNARY_POSTFIX]);
    }
*/
/*
    //% block="PAJ7620U2 serial print result" blockType="command"
    export function  print_Gesture(parameter: any, block: any) {
        
        Generator.addCode(` DFRobot_PAJ7620U2::eGesture_t gesture = paj.getGesture();
        if(gesture != paj.eGestureNone ){
          String description  = paj.gestureDescription(gesture); 
          Serial.println("--------------Gesture Recognition System---------------------------");
          Serial.print("gesture code        = ");Serial.println(gesture);
          Serial.print("gesture description  = ");Serial.println(description);
          Serial.println();}`);
    }
*/
    
}
